# SessionExecute - easy execution of services.
Main reason of creation of this project is to simplify execution of 
services for user. It provides such things:
* Detailed information about services execution(using tables and colors)
* Filters(custom command for each service)
* Apply-to-all(called engage-all) function that allows to apply same
command to each service that defined in configuration files

## Screenshots
![Alt ](Screenshot_1.png?raw=true "sysexec")
![Alt ](Screenshot_2.png?raw=true "sysexec")
![Alt ](Screenshot_3.png?raw=true "sysexec")
