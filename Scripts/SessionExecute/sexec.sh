#!/bin/bash

execute_all_integrated() {
  if [ "$2" != "--disable-help-banner" ]
  then
    l=$(cat "$PWD/dat/sx.defaults" | wc -l)

    echo -e "To disable this banner please use --disable-help-banner option"
    echo -e "Using --engage-all option, services were parsed from:"
    echo -e "\t$PWD/dat/sx.defaults"
    echo -e "Total count of services: \t  \e[1;29m$l\e[m"
    echo -e "Command engaged for all services: \e[1;29m$1\e[m"
    echo -e "All fails are piped to /dev/null, to view fail report"
    echo -e "use sudo systemctl status {SERVICE_NAME}\n"
  fi

  echo -e "        1xSERVER SERVICE STARTUP REPORT   "

  printf '+'; printf -- '-%.0s' {1..49}; printf "+"
  printf '\n|  TYPE   |      NAME    \t|  COMMAND | STATUS |\n' | expand -t 15
  printf '+'; printf -- '-%.s' {1..49}; printf "+\n"

  while read service
  do
      sudo systemctl "$1" "$service" 2>/dev/null

      if [ "$?" != 0  ]
      then
        status="FAIL"
        color="\e[1;31m"
      else
        status="OK"
        color="\e[1;32m"
      fi

      printf "| Service | %s\t|   %5s  | %b%5s\e[m |\n" "$service" "$1" "$color" "$status" | expand -t 15
  done < "dat/sx.defaults"

  printf '+'; printf -- '-%.0s' {1..49}; printf "+\n"
}

execute_using_filter() {
  filtername="$1.sex"
  show_banner="$2"

  if [ "$2" != "--disable-help-banner" ]
    then
      l=$(cat "$PWD/dat/filters/$filtername" | wc -l)

      echo -e "To disable this banner please use --disable-help-banner option"
      echo -e "Using --engage-all option, services were parsed from:"
      echo -e "\t$PWD/dat/filter/$filtername"
      echo -e "Total count of services: \t  \e[1;29m$l\e[m"
      echo -e "Command engaged for all services: \e[1;29m$1\e[m"
      echo -e "All fails are piped to /dev/null, to view fail report"
      echo -e "use sudo systemctl status {SERVICE_NAME}\n"
    fi

    echo -e "        1xSERVER SERVICE STARTUP REPORT   "

    printf '+'; printf -- '-%.0s' {1..49}; printf "+"
    printf '\n|  TYPE   |      NAME    \t|  COMMAND | STATUS |\n' | expand -t 15
    printf '+'; printf -- '-%.s' {1..49}; printf "+\n"

  while read line
  do
    service=$(echo $line | cut -d' ' -f1)
    command=$(echo $line | cut -d' ' -f2)

    sudo systemctl "$command" "$service" 2>/dev/null

    if [ "$?" != 0  ]
    then
      status="FAIL"
      color="\e[1;31m"
    else
      status="OK"
      color="\e[1;32m"
    fi

    printf "| Service | %s\t|  %6s  |  %b%5s\e[m |\n" "$service" "$command" "$color" "$status" | expand -t 15
  done < "dat/filters/$filtername"

  printf '+'; printf -- '-%.0s' {1..49}; printf "+\n"
}

if [ "$1" = "--engage-all" ]
then
	if [ "$2" = "start" ]   || [ "$2" = "stop" ] ||
     [ "$2" = "reload" ]  || [ "$2" = "kill" ] ||
     [ "$2" = "isolate" ] || [ "$2" = "enable" ] ||     
     [ "$2" = "restart" ] || [ "$2" = "try-restart" ] || 
     [ "$2" = "reload-or-restart" ] || [ "$2" = "try-reload-or-restart" ] 
  then
		execute_all_integrated "$2" "$3"
	else
      echo -e "sysexec: \e[1;31merror:\e[m no such systemctl function: $2"
      echo -e "service startup terminated."
      exit 1
	fi
elif [ "$1" = "--filter" ]
then
    if test -f "$PWD/dat/filters/$2.sex"
    then
        execute_using_filter "$2"
    else
      echo -e "sysexec: \e[1;31merror:\e[m $2.sex: No such file or directory"
      echo -e "service startup terminated."
      exit 1
    fi
elif [ "$1" = "--help" ]
then
	  while read line; do
		  echo "$line"
	  done < "dat/help.txt"

elif [ "$1" = "--version" ]
then
  echo -e "sysexec 1.0.0 Beta"
  echo -e "Copyright (C) 1xServer"
  echo -e "This is free software; see the source for copying conditions.  There is NO"
  echo -e "warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE."
else
  echo -e "sysexec: \e[1;31mfatal error:\e[m $1: no such option"
  echo -e "service startup terminated."
  exit 1
fi










