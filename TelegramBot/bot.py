from aiogram            import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils      import executor

import data
import json
import sys

bot = Bot(data.ACCESS_KEY)
dp  = Dispatcher(bot)

@dp.message_handler(commands=['version'])
async def cmd_help(message: types.Message):
    await message.reply(data.SERVER_INFO_VERSION)

@dp.message_handler(commands=['info'])
async def cmd_help(message: types.Message):
    await message.reply(data.INFO_MESSAGE)

@dp.message_handler(commands=['rules'])
async def cmd_help(message: types.Message):
    await message.reply(data.RULES_MESSAGE)

@dp.message_handler(commands=['discord'])
async def cmd_help(message: types.Message):
    await message.reply(data.DISCORD_MESSAGE)

@dp.message_handler(commands=['help'])
async def cmd_help(message: types.Message):
    await message.reply(data.HELP_MESSAGE)

@dp.message_handler(commands=['ip'])
async def cmd_help(message: types.Message):
    await message.reply(data.SERVER_INFO_IP)

@dp.message_handler(commands=['admins'])
async def cmd_help(message: types.Message):
    await message.reply(data.INFO_ADMINS)

@dp.message_handler(commands=['donate'])
async def cmd_help(message: types.Message):
    await message.reply("Дай деняк")

print("Bot is now online...")
executor.start_polling(dp)

